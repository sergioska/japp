var express = require('express');
var logfmt = require("logfmt");
var imagick = require('imagemagick');
var auth = require('./libs/twitter.js');
var mdb = require('./libs/mongo.js');
var utils = require('./libs/utils.js');
var config = require('./config/config.js');

// if true use mongodb
var mongo = true;

var app = express();
var a = new auth.authentication();

if(mongo){
	console.log("mongo ok ...");
	var mongo = new mdb.mongo();
}

app.use(logfmt.requestLogger());

app.use(express.static(__dirname + '/public'));
// public
app.get('/public/*', function(req, res){
	res.sendfile('./public/index.html');
});

app.get('/api/info', function (req, res){
	res.send("Welcome to Bot.ino api service");
});

/** return trends by place_id (yahoo service) */
app.get('/api/trends/:place', function (req, res){
	a.getTrends(req.params.place, function(data){
		var oData = JSON.parse(data);
		// first trend of place id 23424977
		try {
			res.json(oData[0].trends);
		} catch ( e ) {
			console.log(e);
			return false;
		}
	});
});

/** return tweets with an hashtag */
app.get('/api/find/:hashtag', function (req, res){
	a.find(req.params.hashtag, function(data){
		var oData = JSON.parse(data);
		//console.log(oData.statuses[0].id);
		res.json(oData);
	});
});

/** get place_id from yahoo service */
app.get('/api/geo/:query', function (req, res) {
	var u = new utils.utils();
	u.getJSON("http://where.yahooapis.com/v1/places.q('"+req.params.query+"')?appid="+config.yahoo.api_key+"&format=json", function(data){
		console.log(data);
		res.json(JSON.parse(data));
	});
});

/*** test mock for trends api call ***/
app.get('/mock/trends/:place', function (req, res){
	var sData = '[{"name":"#ASCPrettyPlease","query":"%23ASCPrettyPlease","url":"http://twitter.com/search?q=%23ASCPrettyPlease","promoted_content":null},{"name":"#RuinAVideoGameCharacter","query":"%23RuinAVideoGameCharacter","url":"http://twitter.com/search?q=%23RuinAVideoGameCharacter","promoted_content":null},{"name":"#FelizCumpleMessi","query":"%23FelizCumpleMessi","url":"http://twitter.com/search?q=%23FelizCumpleMessi","promoted_content":null},{"name":"#Nomor2Jujur","query":"%23Nomor2Jujur","url":"http://twitter.com/search?q=%23Nomor2Jujur","promoted_content":null},{"name":"#36AñosDeROMANce","query":"%2336A%C3%B1osDeROMANce","url":"http://twitter.com/search?q=%2336A%C3%B1osDeROMANce","promoted_content":null},{"name":"Favorite Avant","query":"%22Favorite+Avant%22","url":"http://twitter.com/search?q=%22Favorite+Avant%22","promoted_content":null},{"name":"Can Mark","query":"%22Can+Mark%22","url":"http://twitter.com/search?q=%22Can+Mark%22","promoted_content":null},{"name":"Ms. Trinidad","query":"%22Ms.+Trinidad%22","url":"http://twitter.com/search?q=%22Ms.+Trinidad%22","promoted_content":null},{"name":"Lil Zane","query":"%22Lil+Zane%22","url":"http://twitter.com/search?q=%22Lil+Zane%22","promoted_content":null},{"name":"Jeepers Creepers","query":"%22Jeepers+Creepers%22","url":"http://twitter.com/search?q=%22Jeepers+Creepers%22","promoted_content":null}]';
	res.json(JSON.parse(sData));
});

app.get('/api/db/trends/:place', function(req, res) {
	// search in mongodb for a trends by place_id
	var place_id = req.params.place;
	_itemHandler(place_id, 
				 null,
				 "trend",
				 _getApiTrends, 
				 function(output){
					res.json(output);
				 }
	);
});

/** return tweets with an hashtag */
app.get('/api/db/find/:hashtag', function (req, res){
	console.log("---FINDER---");
	var hashtag = req.params.hashtag;
	_itemHandler(1,
				 hashtag, 
				 "post", 
				 _getApiPosts,
				 function(output){
					res.json(output);
				 }
	);
});

app.get('/api/retweeters/:id', function(req, res) {
	a.getRetweeters(req.params.id, function(data){
		var oData = JSON.parse(data);
		// first trend of place id 23424977
		try {
			res.json(oData[0]);
		} catch ( e ) {
			console.log(e);
			return false;
		}
	});
})


var port = Number(process.env.PORT || 5000);
app.listen(port, function() {
	console.log("Listening on " + port);
});

var _itemHandler = function(place_id, hashtag, type, api_callback, output_callback){
	console.log("HANDLER");
		mongo.find(place_id, hashtag, type, function(data){
			var lastItem = null;
			var oData = null;
			var weight = 1;
			var d = null;
			var empty = false;
			try{
				d = data.date;
				oData = JSON.parse(data.jsonData);
			} catch(e) {
				empty = true;
				console.log("data empty");
			}
			var api = false;
			var diffTime = 999;
			try{
				diffTime = get_time_difference(new Date(d), new Date(), 'Minutes');
				console.log("DIFFTIME: " + diffTime);
			}catch(e){
				console.log(e);
			}
			// check if data exists in mongodb and if data are fresh
			// if not call api and store data on db
			if(diffTime > 15 || empty==true) {
				console.log("===== IN DIFF =====");
				api_callback(place_id, hashtag, output_callback);
			} else {
				console.log("FROM DB");
				try {
					if(hashtag!=null){
						console.log("++++ HASHTAG ++++");
						//console.log(oData);
						output_callback(oData);
					} else {
						console.log("++++ TRENDS ++++");
						output_callback(oData[0].trends);
					}
				} catch(e) {
					console.log("Dataset is empty!");
				}	
			}
		});
}

function get_time_difference(earlierDate,laterDate)
{
    var nTotalDiff = laterDate.getTime() - earlierDate.getTime();
    var diff = Math.abs(nTotalDiff);
    return Math.round(diff/1000/60);
}

_getApiTrends = function(place_id, hashtag, output_callback) {
			a.getTrends(place_id, function(data_from_api){
				console.log("GET FROM API");
				try {
					mongo.insert(place_id, null, "trend", data_from_api, function(lastItem){
						var oData = JSON.parse(lastItem);
						//console.log(oData[0].trends);
						output_callback(oData[0].trends);
					});
				} catch(e) {
					console.log("Problem on mongodb insert");
				}

			});	
}

_getApiPosts = function(place_id, hashtag, output_callback) {
				console.log("_getApiPosts");
				a.find(hashtag, function(data_from_api){
					console.log("FIND FROM API");
					try {
						mongo.insert(place_id, hashtag, "post", data_from_api, function(lastItem){
							var oData = JSON.parse(lastItem);
							//console.log(oData);
							output_callback(oData);
						});
					} catch(e) {
						console.log("Problem on mongodb insert post");
					}
				});
}

