// core.js
(function(){

	var botino = angular.module('appBot', ['ui.bootstrap']);
	var currentZone = {'it': "23424853", 
					   'fr': "23424819", 
					   'gb': "23424975",
					   'us': "23424977",
					   'ca': "23424775",
					   'br': "23424768",
					   'ge': "23424829", 
					   'es': "23424950",
					   'be': "23424757",
					   'ne': "23424909"};

	botino.controller('HashtagController', function($scope, $http){

		$scope.trends = [];
		$scope.loader = false;

		$scope.getTrends = function(zone){
			$http.get("http://treendi.herokuapp.com/api/db/trends/"+zone)
			//$http.get("http://localhost:5000/api/trends/"+zone)
			//$http.get("http://localhost:5000/api/db/trends/"+zone)
				 		.success(function(data, status, headers, config) {
				 			$scope.loaded = true;
							angular.forEach(data, function(value, index){
								$scope.trends.push(value);
							});
							$scope.select($scope.trends[0].name, 1);
				 		})
				 		.error(function(data) {
				 			console.log("ERROR");
				 	});
		}

		$scope.getTrends(currentZone['us']);

		$scope.select = function(tag, index) {	
			$('button').attr('class', 'btn');
			$("#btn-"+index).attr('class', 'btn btn-primary');
			$scope.lastTweetsTmp = [];
			$scope.lastTweets = [];
			$scope.slides = [];
			if(tag.indexOf("#")==0)
				tag = tag.substring(1);
			tag = tag.replace(" ", "%20");
			//$http.get("http://localhost:5000/api/db/find/"+tag)
			$http.get("http://treendi.herokuapp.com/api/db/find/"+tag)
	 		.success(function(data, status, headers, config) {
	 			$scope.loader = true;
	 			$scope.myInterval = 3000;
	 			var x = 0;
				angular.forEach(data, function(value, index){
					$scope.lastTweetsTmp.push(value);
				});
				$scope.lastTweets = $scope.lastTweetsTmp[0];
				$("#btn-"+index).attr('class', 'btn btn-primary');
				angular.forEach($scope.lastTweets, function(value, index){
					if(value.entities.hasOwnProperty('media')==true){
						$scope.slides.push(value.entities.media[0].media_url);
					}
					/*
					if(value.entities.media)
						console.log(value.entities.media);
					$scope.slides.push(value.entities);*/
				});
				console.log($scope.slides);
				//console.log($scope.lastTweets);
	 		})
	 		.error(function(data) {
	 			console.log("ERROR");
	 		});
		}

		$scope.selectZone = function(zone) {
			$scope.loaded = true;
			$('#flag a[href="#'+zone+'"]').tab('show');
			$scope.trends = [];
			$scope.getTrends(currentZone[zone]);
		}
	});

	botino.filter('parseUrl', function($sce) {
		var //URLs starting with http://, https://, or ftp://
		replacePattern1 = /(\b(https?|ftp):\/\/[-A-Z0-9+&@#\/%?=~_|!:,.;]*[-A-Z0-9+&@#\/%=~_|])/gim,
		//URLs starting with "www." (without // before it, or it'd re-link the ones done above).
		replacePattern2 = /(^|[^\/])(www\.[\S]+(\b|$))/gim,
		//Change email addresses to mailto:: links.
		replacePattern3 = /(\w+@[a-zA-Z_]+?\.[a-zA-Z]{2,6})/gim;
 
		return function(text, target, otherProp) {
			angular.forEach(text.match(replacePattern1), function(url) {
				text = text.replace(replacePattern1, '<a href="$1" target="_blank">$1</a>');
			});
			angular.forEach(text.match(replacePattern2), function(url) {
				text = text.replace(replacePattern2, '$1<a href="http://$2" target="_blank">$2</a>');
			});
			angular.forEach(text.match(replacePattern3), function(url) {
				text = text.replace(replacePattern3, "<a href=\"mailto:$1\">$1</a>");
			});
			text = $sce.trustAsHtml(text);	
		return text;
		};
	});

	botino.config(function($httpProvider) {
		$httpProvider.interceptors.push(function($q, $rootScope) {
			return {
				'request': function(config) {
					$rootScope.$broadcast('loading-started');
					return config || $q.when(config);
				},
				'response': function(response) {
					$rootScope.$broadcast('loading-complete');
					return response || $q.when(response);
				}
			};
		});
	});

	botino.directive("loadingIndicator", function() {
            return {
                restrict : "A",
                template: "<div>Loading...</div>",
                link : function(scope, element, attrs) {
                    scope.$on("loading-started", function(e) {
                        element.css({"display" : ""});
                    });

                    scope.$on("loading-complete", function(e) {
                        element.css({"display" : "none"});
                    });

                }
            };
        });

})();
