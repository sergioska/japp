// test.js

// it's a good example of nested callback function

var express = require('express');

var auth = require('./libs/twitter.js');

var app = express();
var a = new auth.authentication();

/** return trends by place_id (place_id as yahoo service way) */
// _handler is a function with 3 parameters:
// the first is a simple variable
// the second is a callback function 
// the third is an anonimous callback function
app.get('/api/trends/:place', function (req, res){
	_handler(req.params.place, 
		  _getApiTrends,
		  function(data){
				res.json(data);
		  }
	);
});

app.get('/api/dummy/:place', function (req, res){
	_handler(req.params.place,
	_dummy,
	function(data){
		res.json(data);
	});
});

_handler = function(place, api_callback, output_callback) {
	api_callback(place, output_callback);
}

_getApiTrends = function(place, output_callback) {
	a.getTrends(place, function(data){
		var oData = JSON.parse(data);
		// first trend of place id 23424977
		//console.log(oData[0].trends[0].name);
		//res.send("HELLO WORLD!" + oData[0].trends[0].name);
		try {
			output_callback(oData[0].trends);
			//res.json(oData[0].trends);
		} catch ( e ) {
			console.log(e);
			return false;
		}
	});
}

_dummy = function(place, output_callback) {
	console.log("I'm dummy");
	output_callback("I'm dummy" + place);
};


var port = Number(process.env.PORT || 5000);
app.listen(port, function() {
	console.log("Listening on " + port);
});