# README

jApp is a single-page web application developed in javascript using node.js + express.js on server side (to web service implementation) and angular.js, bootstrap on client side. This is only a part of a project hosted on heroku at follow link:

http://www.treendi.com

## Features

Launch the service on server side, a client can send some request to a node.js service express.js based:

```
/api/trends/<place_id> #trends in a place
/api/find/<string> #search for a word (hashtag, ecc ...)  
```

Go ahead to start service with:

```
node app.js
```

To add config parameter (for twitter access token and for mongodb connection) you need to add a directory at the same level of libs named 'config'; after make a file named 'config.js' with this content:


```
// configuration file.
var config = {}

config.twitter = {};
config.yahoo = {};
config.mongo = {};

config.twitter.api_key = "";
config.twitter.api_secret = "";
config.twitter.oauth_access_token = "";
config.twitter.oauth_access_token_secret = "";

config.yahoo.api_key = "";

config.mongo.username = "";
config.mongo.password = "";
config.mongo.uri  = "mongodb://";

module.exports = config;
```

So add the right configuration parameters.