//twitter.js
var config = require('../config/config.js');

var util = require('util')
var OAuth = require('oauth');

authentication = function(){
	this.basePath = "https://api.twitter.com/1.1/";
	this.request_token_url = "https://api.twitter.com/oauth/request_token";
	this.access_token_url = "https://api.twitter.com/oauth/access_token";
	this.api_key = config.twitter.api_key;
	this.api_secret = config.twitter.api_secret;
	this.oauth_access_token = config.twitter.oauth_access_token;
	this.oauth_access_token_secret = config.twitter.oauth_access_token_secret;
	this.oauth = new OAuth.OAuth( 
							this.request_token_url,
                  			this.access_token_url,
                  			this.api_key,
                  			this.api_secret,
                  			"1.0",
                  			null,
                  			"HMAC-SHA1"
                  			);

	authentication.prototype.getTrends = function(place_id, output_callback){
		this.oauth.get(this.basePath + "trends/place.json?id=" + place_id, 
						this.oauth_access_token, 
						this.oauth_access_token_secret, 
						function (error, data, response) {
	    					if(error)
	    						console.log(error);
	    					output_callback(data);
	        			}
	    );
	}

	authentication.prototype.find = function(hashtag, output_callback){
		this.oauth.get(this.basePath + "search/tweets.json?q="+hashtag+"&result_type=mixed", 
						this.oauth_access_token, 
						this.oauth_access_token_secret, 
						function (error, data, response) {
	    					if(error)
	    						console.log(error);
	    					output_callback(data);
	        			}
	    );
	}

	authentication.prototype.getMentions = function(output_callback){
		this.oauth.get(this.basePath + "statuses/mentions_timeline.json?", 
						this.oauth_access_token, 
						this.oauth_access_token_secret, 
						function (error, data, response) {
	    					if(error)
	    						console.log(error);
	    					output_callback(data);
	        			}
	    );	
	}

	authentication.prototype.getRetweeters = function(id, output_callback){
		this.oauth.get(this.basePath + "statuses/retweets/"+id+".json?", 
						this.oauth_access_token, 
						this.oauth_access_token_secret, 
						function (error, data, response) {
	    					if(error)
	    						console.log(error);
	    					output_callback(data);
	        			}
	    );	
	}


}

exports.authentication = authentication;