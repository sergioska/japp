// schema.js
var mongoose = require('mongoose')
  , Schema = mongoose.Schema;


var HashtagSchema = new Schema({
  type: String,
  jsonData: String,
  date: {type: Date, default: Date.now},
  place_id: Number,
  hashtag: {type: String, default: null}
});

var Hashtag = mongoose.model('Hashtag', HashtagSchema);

module.exports = {
  Hashtag: Hashtag
}

