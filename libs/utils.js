// utils.js
var request = require('request');

utils = function() { }

utils.prototype.getJSON = function(url, output_callback){
	request(url, function (err, response, body) {
		output_callback(body);
	});
}

exports.utils = utils;