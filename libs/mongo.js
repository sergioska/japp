// mongo.js
var config = require('../config/config.js');
var mongoose = require('mongoose');
var Hashtag = require("./schema.js").Hashtag;

mongo = function(){
	var options = {
		db: { native_parser: true },
		server: { poolSize: 5 },
		replset: { rs_name: 'myReplicaSetName' },
		user: config.mongo.username,
		pass: config.mongo.password
	}

	this.connection = mongoose.connect(config.mongo.uri, options, function(err, res){
		if (err) console.log("error mongodb connection");
		else console.log("mongodb connection success");
	}); 

}

mongo.prototype.insert = function(place, hashtag, itemType, input, output_callback) {	
	var d = new Hashtag();
	d.type = itemType;
	d.jsonData = input;
	d.date = Date.now();
	d.place_id = place;
	if(hashtag!=null)
		d.hashtag = hashtag;
	d.save(function (err, product, numberAffected) { 
		if (err) throw err;
		console.log("INSERT OK" + hashtag + " - " + itemType);
		//console.log(product.jsonData);
		output_callback(product.jsonData);
		//mongoose.disconnect();
	});
}

mongo.prototype.find = function(place, hash, itemType, output_callback) {
	if(hash!=null){
		var d = Hashtag.findOne({hashtag: hash, type: itemType}, {}, { sort: { 'date' : -1 } }, function(err, post) {
  			output_callback(post);
		});
	} else {
		var d = Hashtag.findOne({place_id: place, type: itemType}, {}, { sort: { 'date' : -1 } }, function(err, post) {
  			output_callback(post);
		});
	}
}

exports.mongo = mongo;
